package ubb.electivo.proyecto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

@SpringBootApplication
@EnableSwagger2WebMvc
public class GatewayRckxt1Application {

	public static void main(String[] args) {
		SpringApplication.run(GatewayRckxt1Application.class, args);
	}

}
