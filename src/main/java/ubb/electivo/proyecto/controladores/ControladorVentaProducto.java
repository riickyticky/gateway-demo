package ubb.electivo.proyecto.controladores;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ubb.electivo.proyecto.producto.ProductoDTO;
import ubb.electivo.proyecto.servicios.VentaProductoServicio;

@RestController
public class ControladorVentaProducto {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private VentaProductoServicio ventaProductoServicio;
	
	@GetMapping("/leer-todos-productos")
    public Object leerTodosProductos(HttpServletRequest request) {
		
		ResponseEntity<Object> rs = null;	
        try {
            rs = new ResponseEntity<>(this.ventaProductoServicio.leerTodoProductos(request), HttpStatus.OK);
        }catch (Exception ex){
            logger.error(ex.getMessage(),ex);
            rs = new ResponseEntity<>(ex.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return rs.getBody();
    }
	
	@PostMapping("/crear-productos")
    public Object crearProductos(@RequestBody ProductoDTO productoDto, HttpServletRequest request) {
		
		ResponseEntity<Object> rs = null;	
        try {
            rs = new ResponseEntity<>(this.ventaProductoServicio.crearProductos(productoDto, request), HttpStatus.OK);

            if(rs.getBody().toString().equals("<400 BAD_REQUEST Bad Request,[]>")) {
            	return rs = new ResponseEntity<>("Error al crear producto", HttpStatus.INTERNAL_SERVER_ERROR);
            }
            	
        }catch (Exception ex){
            logger.error(ex.getMessage(),ex);
            rs = new ResponseEntity<>(ex.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return rs.getBody();
        	
    }
}
