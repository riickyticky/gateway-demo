package ubb.electivo.proyecto.producto;

public class ProductoDTO {
	
	private String nombreProducto; 
	private Integer precioProducto;
	private Integer activo;
	private Long tipoProductoId;
	public String getNombreProducto() {
		return nombreProducto;
	}
	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}
	public Integer getPrecioProducto() {
		return precioProducto;
	}
	public void setPrecioProducto(Integer precioProducto) {
		this.precioProducto = precioProducto;
	}
	public Integer getActivo() {
		return activo;
	}
	public void setActivo(Integer activo) {
		this.activo = activo;
	}
	public Long getTipoProductoId() {
		return tipoProductoId;
	}
	public void setTipoProductoId(Long tipoProductoId) {
		this.tipoProductoId = tipoProductoId;
	}
	public ProductoDTO(String nombreProducto, Integer precioProducto, Integer activo, Long tipoProductoId) {
		super();
		this.nombreProducto = nombreProducto;
		this.precioProducto = precioProducto;
		this.activo = activo;
		this.tipoProductoId = tipoProductoId;
	}
	
	public ProductoDTO() {}
}
