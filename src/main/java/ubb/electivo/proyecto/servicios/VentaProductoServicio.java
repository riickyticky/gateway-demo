package ubb.electivo.proyecto.servicios;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import ubb.electivo.proyecto.producto.ProductoDTO;
import ubb.electivo.proyecto.util.ConstantesUri;
import ubb.electivo.proyecto.util.RestTemplateUtil;

@Service
public class VentaProductoServicio {
	
	@Autowired
	RestTemplateUtil restTemplate;
	
	@Value("${servicio.producto.url}")
	String servicioProductoUrl;
	
	public Object leerTodoProductos(HttpServletRequest request) {
		return this.restTemplate.callResource(servicioProductoUrl + ConstantesUri.LEER_PRODUCTOS, HttpMethod.GET, request);
	}
	
	public Object crearProductos(ProductoDTO productoDto, HttpServletRequest request) {
		return this.restTemplate.callResource(servicioProductoUrl + ConstantesUri.CREAR_PRODUCTOS, HttpMethod.POST,productoDto,request);
	}
}
