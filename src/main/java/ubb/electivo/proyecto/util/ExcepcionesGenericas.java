package ubb.electivo.proyecto.util;
import org.springframework.http.HttpStatus;

public class ExcepcionesGenericas extends Exception {
    private static final long serialVersionUID = 6016642833009371710L;
    private final HttpStatus httpStatus;
    public ExcepcionesGenericas(String message, HttpStatus httpStatus){
        super(message);
        this.httpStatus = httpStatus;
    }
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}