package ubb.electivo.proyecto.util;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


@Service
public class RestTemplateUtil {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	RestTemplate restTemplate;
	
	public RestTemplateUtil() {
		restTemplate = new RestTemplate();
	}
	
	
	//Sin paginación - GET y DELETE
	public ResponseEntity<String> callResource(String url, HttpMethod type, HttpServletRequest request) {
		
		final String authHeader = request.getHeader(Constantes.AUTHORIZATION);
	    HttpHeaders headers = new HttpHeaders();
	    headers.set(Constantes.AUTHORIZATION, authHeader);
	    
	    HttpEntity<String> requestEntity = new HttpEntity<>(headers);
	    ResponseEntity<String> responseEntity =  null;
	    
		 try {
			 responseEntity = restTemplate.exchange(url, type, requestEntity, String.class);
			 
			 if (responseEntity.getBody() == null && !type.equals(HttpMethod.DELETE)) {
	                logger.error(Constantes.ERROR_GET_ENT_ALL);
	                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	         }
			 
		} catch (RestClientException e) {
			logger.error(e.getMessage());
			if(type.equals(HttpMethod.DELETE))
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return responseEntity;
    } 
	
	

	
	//Para metodos put y post que reciben un objeto que no es un json
	public ResponseEntity<String> callResource(String url, HttpMethod type, Object object, HttpServletRequest request) {
        
		final String authHeader = request.getHeader(Constantes.AUTHORIZATION);
	    HttpHeaders headers = new HttpHeaders();
	    headers.set(Constantes.AUTHORIZATION, authHeader);

        HttpEntity<Object> requestEntity = new HttpEntity<>(object, headers);
        ResponseEntity<String> responseEntity = null;

		 try {
			 responseEntity = restTemplate.exchange(url, type, requestEntity, String.class);
			 if (responseEntity.getBody() == null) {
                logger.error(Constantes.ERROR_GET_ENT_ALL);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	         } 
			 
		} catch (RestClientException e) {
			logger.error(e.getMessage());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return responseEntity;
    }
	
}	
